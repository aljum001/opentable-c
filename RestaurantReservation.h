/* A2 RestaurantReservation.h file : below has the declarations for private and public methods of the class
   restaurant reservation.*/
#ifndef RESTAURANTRESERVATION_H
#define RESTAURANTRESERVATION_H
#include "Restaurant.h"
#include "Reservation.h"
#include <vector>
#include <string>
using namespace std;

class RestaurantReservation
{ private : // creats a vector of objects of class restaurants
  vector<Restaurant>restaurants;
 public :
  void ProcessTransactionFile(string); // function to read in from file
  void CreateNewRestaurant(string, string, string, string, int);
  //creates new restaurant
  void PrintAllRestaurants() const;
  //prints all of the restaurants info
  void FindTable(const string&,const string&,const int&,const int&) const;
  //find table func
  void FindTableAtRestaurant(const string&,const int&) const;
  // find table at a specific restaurant func
  void MakeReservation(string, string, string, int, int);
  // make reservation function
  void PrintRestaurantReservation(const string&) const;
  // prints the reservation function
};


#endif
