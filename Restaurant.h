/*A2 Restaurant.h : below contains all the declarations for the class Restaurant that will have new private and public functions and will also be using
  the reservation class*/
#ifndef RESTAURANT_H
#define RESTAURANT_H
#include "Reservation.h"
#include <string>
#include <vector>
using namespace std;
class Restaurant
{
 private: //private attributes for the information beind held in the class
  string restaurantName;
  string restaurantAddress;
  string restaurantCity;
  string restaurantType;
  int availableSeats[4];
  vector<Reservation>reservations; // vector of the class reservations
 public: 
  Restaurant();
  Restaurant(string, string, string, string, int); // overloaded constructer
  void makeReservation(string, string, int, int); //make reservation func
  void printRestaurant() const; //print restaurant info func
  string getRName() const; // returns restaurant name 
  string getRCity() const; // returns city the restaurants is in
  string getRType() const; // returns the type of the restaurant
  int getRGroup(int) const; // returns the number of seats at a specific time
  void printReservation() const; // prints reservation
  int getSeats(const int&) const;// returns the number of seats at a specific time
};
#endif
