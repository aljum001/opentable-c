/* A2 Reservation.cpp : this file has all the definitions of the functions that are in the class Reservation. */
#include "Reservation.h"
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
long Reservation ::  nextReservationNum = 100; //first reservation num
Reservation :: Reservation():reservationNum(nextReservationNum)
{ // default constructer that assigns all the values and assigns 
  // reservation number to the next reservation num
  contactName = "";
  contactPhone = "";
  groupSize = 0;
  reservationTime = 0;
  nextReservationNum += 10;
}

Reservation :: Reservation(string cName, string cPhone, int rGroup,
			   int rTime):reservationNum(nextReservationNum)
{ //over loaded constructer that does the same as the default but assigns
  //to the value that are sent in when the function is called 
  contactName = cName;
  contactPhone = cPhone;
  groupSize = rGroup;
  reservationTime = rTime;
  nextReservationNum += 10;
}

void Reservation :: printReservation() const
{ //this function prints out the information of the reservation
  // in a neat format
  cout << left << setw(14) << reservationNum << setw(10) 
       <<  contactName << setw(14) << contactPhone 
       << setw(7)<< groupSize << reservationTime << ":00 PM" << endl;
}


long Reservation :: getRNum() const
{ // returns the reservation number
  return reservationNum;
}
