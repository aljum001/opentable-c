/*A2 RestaurantReservation.cpp file : below are all the defintions 
  of the class Restaurant Reservation*/
#include "Restaurant.h"
#include "Reservation.h"
#include "RestaurantReservation.h"
#include <vector>
#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

void RestaurantReservation :: ProcessTransactionFile(string fileName) 
{ // this function opens up a file and process the information based
  // on the command that it receives
  string command, rName, rAddress, rCity, rType, cName, cPhone;
  int rCapacity, rGroup, rTime;
  ifstream fin; // file stream varaible 
  fin.open(fileName); //opening file with the name fileName
  if (!fin) // condition to check if file opens
    {
    cout << "File could not be opened." << endl;
    }
  while (fin >> command) // reads in the command from file
    {
      if (command == "CreateNewRestaurant") 
	{ //if the command = CreateNewRestaurant, input the information
	  //below and call the function CreateNewRestaurant with the info
	  fin >> rName >> rAddress >> rCity >> rType >> rCapacity;
	  CreateNewRestaurant(rName, rAddress, rCity, rType, rCapacity);
	}
      else if (command == "FindTable")
	{ //if the command = FindTable input the info below and
	  // call the function findtable with the info
	  fin >> rCity >> rType >> rGroup >> rTime;
	  FindTable(rCity, rType, rGroup, rTime);
	}
      else if (command == "FindTableAtRestaurant")
	{ //if the command = FindTableAtRestaurant input the info below
	  //and call the restaurant with the info
	  fin >> rName >> rGroup;
	  FindTableAtRestaurant(rName, rGroup);
	}
      else if (command == "MakeReservation")
	{ //if the command = MakeReservation input the info below
	  // and call the MakeReservation function with the info 
	  fin >> rName >> cName >> cPhone>> rGroup >> rTime;
	  MakeReservation(rName, cName, cPhone, rGroup, rTime);
	}
      else if (command == "PrintAllRestaurants")
	{ //if the command = PrintAllRestaurants call the function
	  PrintAllRestaurants(); // PrintAllRestaurant
	}
      else if (command == "PrintRestaurantReservations")
	{ //if the command = PrintRestaurantReservation
	  //input into rName and call PrintReestaurantReservation 
	  cout << endl; // function with rName
	  fin >> rName;
	  PrintRestaurantReservation(rName);
	}

    }
}
void RestaurantReservation :: 
CreateNewRestaurant(string rName, string rAddress,
		    string rCity,string rType, int rCapacity)
{ // function that creats an object of Restaurant and
  // calls the overloaded constructor and it
  // pushes the object into the restaurants vector
  Restaurant R1(rName, rAddress, rCity, rType, rCapacity);
  restaurants.push_back(R1);
}

void RestaurantReservation ::
FindTable(const string& rCity,const string& rType,const int& rGroup, 
	  const int& rTime) const
{ // constant function that finds tables in a city
  int counter = 0; 
  bool flag = false; 
  for (int i = 0; i <restaurants.size(); i++)
    { //loop to check the entire vector
      if      (	(rCity == restaurants[i].getRCity()) && 
		(rType == restaurants[i].getRType()) && 
		(rGroup <= restaurants[i].getRGroup(rTime))) 
	{ //condition above is to check if the name,type,
	  counter++; // and the number of seats match the vector
	  if (counter == 1)
	    { //prints the table in a neat manner
	      cout << "You may reserve a table for " << rGroup 
		   << " at " << rTime << " pm at:" << endl;
	    } //if the above condition is true call the function
	  // to return the restaurant name
	  cout << restaurants[i].getRName() << endl;
	  flag = true;
	}	
    }
  if (flag == false)
    { // if the main condition above is false print below
      cout << "No Restaurant can accomodate such a group at this time, ";
      cout << "check another time." << endl;
    }
  cout << endl;  
}

void RestaurantReservation :: FindTableAtRestaurant(const string& rName, const int& rGroup)
const
{ // constant function that finds a table at a specific restaurant
  int counter = 0;
  bool flag = false;
  for (int i = 0; i < restaurants.size(); i++)
    { // loop to check the entire restaurant vector
      if (rName == restaurants[i].getRName())
	{ // condition to check if the name exists in the restaurant 
	for(int c = 0; c < 4; c++)
	  {// if it exists run a loop for 4	      
	    if (rGroup <= restaurants[i].getSeats(c))	
	      { // condition to check if there are availableseats
		  counter++;
		  if (counter == 1)
		    { 
		      cout << "You can reserve a table for " 
			   << rGroup << " at " << rName
			   << " at ";
		    }
		  flag = true; //if it is then print out the time
		  cout << c+5 << " PM "; //of the available tables
		}
	       	
	  }
	cout << endl;
	}
    }
  if (flag == false)
    { //if it does not find any of the above print below
      cout << "No available seating at this time." << endl;
    }
}
		      
	
void RestaurantReservation :: MakeReservation(string rName, string cName,
					      string cPhone, int rGroup,
					      int rTime)
{//this function makes a reservation if the conditions below are met
  for (int i = 0; i < restaurants.size(); i++)
    {//loop for to check the entire vector of restaurants
      if (rName == restaurants[i].getRName())
	{//condition to check if the restaurants exists
	 //if the condition is met call the makereservation func
	 //with the variables below
	  restaurants[i].makeReservation(cName, cPhone, rGroup, rTime);
	  cout << "A reservation has been made for " << cName 
	       << " at " << rTime << endl;
	}
    }
  cout << endl;
}


void RestaurantReservation :: PrintAllRestaurants() const
{//below is the function to print all the restaurants in the
  //restaurant vector in a neat table
  cout <<  "Below are all the Restaurants near you:" << endl;
  cout << left << setw(17) << "Restaurant" << setw(18)
       << "Address" << setw(10) << "City" << setw(9)
       << "Type" << "Capacity" << endl;
  cout << "--------------------------------------------------------------";
  cout << endl;
  for (int i = 0; i < restaurants.size(); i++)
    { //loop to print all the restaurants
      restaurants[i].printRestaurant();
    }
  cout << endl;
}

void RestaurantReservation :: PrintRestaurantReservation(const string& rName) const 
{//this function prints all the reservations for a specific restaurant
  bool flag = false;  
  for (int i = 0; i < restaurants.size(); i++)
    {//loop for the entire restaurant vector
      if (rName == restaurants[i].getRName())
	{//condition to check the name of the restaurant
	  //if it does then print the reservations in
	  flag = true;//a neat table below 
	  cout << "Below are the reservations for " << rName << endl;
	  cout << left << setw(14) << "Reservation" << setw(10)
	       << "Contact" << setw(14) << "Phone" << setw(7)
	       << "Group" << "Time" << endl;
	  cout << "----------------------------------------------------" << endl;
	  restaurants[i].printReservation();
	  cout << endl;
	}
    }
  if (flag == false) 
    {//if not print restuarant does not exist
      cout << rName << " does not exist." << endl;
    }
    
  cout << endl;
}
