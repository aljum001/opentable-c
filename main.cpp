/*A2 main.cpp file. this file will create an of restaurant reservations and 
  and starts the restaurant directory*/
#include "Restaurant.h"
#include "Reservation.h"
#include "RestaurantReservation.h"
#include <vector>
#include <iostream>

using namespace std;

int main()
{
  RestaurantReservation openTable; //creates an object of 
  //restaurant reservation and calls the transaction
  //func to open the file and get the program running
  openTable.ProcessTransactionFile("TransactionFile.txt");
  return 0;
}
