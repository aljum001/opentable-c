/*A2 Reservation.h : Below is the class declaration for the clas RESERVATION
this class has private and public methods.
the function of this class is to store information when a reservation is 
made for a restaurant.*/ 
#ifndef RESERVATION_H
#define RESERVATION_H
#include <string>
using namespace std;

class Reservation // private methods include the attributes that will be 
{ private: //used to store information for the reservation
  const long reservationNum; // constant variable that holds the
  string contactName;        // reservation number
  string contactPhone;
  int groupSize;
  int reservationTime;
  static long nextReservationNum; // variable that changes the next 
 public:                          // reservation number
  Reservation();
  Reservation(string, string, int, int); //overloaded constructer 
  void printReservation() const; //prints the reservation info
  long getRNum() const; //returns reservation number when called 
};
#endif
