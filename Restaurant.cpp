/*A2 Restaurant.cpp : below has all the definitions of the restaurant
  class*/
#include "Restaurant.h"
#include "Reservation.h"
#include <iostream>
#include <vector>
#include <string>
#include <iomanip>
using namespace std;
Restaurant :: Restaurant()
{ //default constructor that initializes the values of the restaurant 
  // class
  restaurantName = "";
  restaurantAddress = "";
  restaurantCity = "";
  restaurantType = "";
  for (int i =0; i < 4; i ++)
    { //initializing the available seats array to 0 for each 
      //hour 
      availableSeats[i]=0;
    }
}

Restaurant :: Restaurant(string rName, string rAddress, string rCity,
			 string rType, int rCapacity)
{ //overloaded constructor that takes in values and assigns them to 
  //restaurant class
  restaurantName = rName;
  restaurantAddress = rAddress;
  restaurantCity = rCity;
  restaurantType = rType;
  for (int i = 0; i < 4; i++)
    { // assigns the available seats for hour 5-8 to rCapacity;
      availableSeats[i] = rCapacity;
    }
}


void Restaurant :: printRestaurant() const
{ //prints the restaurant values in a neat format
  cout << left << setw(17) << restaurantName << setw(18)
       << restaurantAddress << setw(10) << restaurantCity
       << setw(9)<< restaurantType  <<  availableSeats[0] << endl;
}

void Restaurant :: makeReservation( string cName, 
				   string cPhone, int rGroup, int rTime)
{ //this functions creats an object of the class Reservation
  //and calls the overloaded reservation constructor. 
  //then that object is pushed into the reservation vector
  Reservation R1(cName, cPhone, rGroup, rTime);
  reservations.push_back(R1);
  availableSeats[rTime - 5] -= rGroup; 
  //subtracting the number of seats for the hour that the
  //reservation is being made for
}

string Restaurant :: getRName() const
{ // returns the name of the restaurant
  return restaurantName;
}

string Restaurant :: getRCity() const
{ // returns the city
  return restaurantCity;
}

string Restaurant :: getRType() const
{ // returns the type of the restaurant
  return restaurantType;
}

int Restaurant ::  getRGroup(int rTime) const
{ // returns the number of seats at a specific time
  return availableSeats[rTime-5];
}

void Restaurant :: printReservation() const
{ //loop to call the print reservation function in 
  //reservation class to print all the reservations 
  // in the vector
  for (int i = 0; i < reservations.size(); i++)
    {
      reservations[i].printReservation();
    }
}

int Restaurant :: getSeats(const int& index) const
{ // returns the number of seats at a specific index;
  return availableSeats[index];
}
